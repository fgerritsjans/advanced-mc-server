import https from 'https'
import fs from 'fs'
import { spawn } from 'child_process'
import EventEmitter from 'events'

class Paper extends EventEmitter {
  api = 'https://papermc.io/api/v1'

  constructor(version) {
    super()

    this.update(version).then(({ file, version, build }) => {
      const child = spawn('java', [
        '-Xmx512M',
        '-Xms256M',
        '-jar',
        '../' + file,
        'nogui'
      ], {
        cwd: 'server/'
      })

      child.on('error', error => {
        console.error('Failed to start child process\n', error)
      })

      child.stdout.on('data', data => this.emit('message', data));
      child.stderr.on('data', data => this.emit('error', data));
    })
  }

  update(version) {
    return new Promise((resolve, reject) => {
      new Promise((res, rej) => {
        if (version) {
          res(this.latestBuilds(version))
        } else {
          this.versions.then(({ versions }) => {
            res(this.latestBuilds(versions[0]))
          })
        }
      }).then(({ version, build }) => {
        this.download(version, build).then(file => {
          resolve({
            file,
            version,
            build
          })
        })
      })
    })
  }

  /**
   * Get available Minecraft versions
   * @returns {Promise}
   */
  get versions() {
    return new Promise((resolve, reject) => {
      https
        .get(this.api + '/paper', res => {
          let chunk = []

          res.on('data', d => chunk.push(d))
          res.on('end', () => {
            try {
              const data = JSON.parse(chunk.join(''))
              resolve(data)
            } catch (e) {
              reject('Invalid API response')
            }
          })
        })
        .on('error', reject)
    })
  }

  latestBuilds(version) {
    return new Promise((resolve, reject) => {
      https.get(this.api + '/paper/' + version + '/latest', res => {
        let chunk = []

        res.on('data', d => chunk.push(d))
        res.on('end', () => {
          try {
            resolve(JSON.parse(chunk.join('')))
          } catch (e) {
            reject('Invalid API response')
          }
        })
      })
    })
  }

  download(version, build = 'latest') {
    return new Promise((resolve, reject) => {
      const filename = `${version}_${build}.jar`
        .replace(/[^a-z0-9._]/gi, '.')
        .toLowerCase()

      const filepath = './dist/' + filename
      const url = `${this.api}/paper/${version}/${build}/download`

      fs.stat(filepath, (err, stat) => {
        if (!err) {
          resolve(filepath)
        } else if (err.code === 'ENOENT') {
          https
            .get(url, res => {
              if (res.headers['content-type'] === 'application/java-archive') {
                const file = fs.createWriteStream(filepath)

                res.pipe(file)

                file.on('finish', () => {
                  file.close()
                  resolve(filepath)
                })
              } else {
                reject()
              }
            })
            .on('error', reject)
        }
      })
    })
  }
}

export default Paper
